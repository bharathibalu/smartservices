from flask.ext.restful import Resource, marshal, fields, reqparse
from sqlalchemy.orm.exc import NoResultFound
from app import auth
from app import api
from app.database import TaskClass
from app.database import ServiceProviderClass
from app.database import CustomerClass
from app.database import ServicesClass
from app.database import ServicesByServiceProviderClass
from app.database import AreasClass
from app.database import AreasForUserClass
from app.database import ServiceProviderInTaskClass
from app.database import ReviewsClass
from flask import g
from app import db
from ServiceProvider_handler import serviceProvider_fields
from Review_handler import review_fields
from misc import serviceProviderInTask_fields

#Fields for sending data to the front-end
task_fields = {
	'task_id' : fields.Integer,
	'user_id' : fields.Integer,
	'customer_id' : fields.Integer,
	'area' : fields.String,
	'type' : fields.String, #one-time or monthly
	'service' : fields.String,
	'evening_only' : fields.String,
	'weekend_only' : fields.String,
	'starting_price' : fields.Integer,
	'ending_price' : fields.Integer,
}

class task(Resource):
	decorators = [auth.login_required]
	
	def marshal_items(self, items, fields):
		return map(lambda x: marshal(x,fields), items)
			
	def __init__(self):
		#Parse input data from front-end
		self.reqparser = reqparse.RequestParser()
		self.reqparser.add_argument('area',type = unicode,location = 'json')
		self.reqparser.add_argument('type',type = unicode,location = 'json')
		self.reqparser.add_argument('service',type = unicode,location = 'json')
		self.reqparser.add_argument('evening_only',type = unicode,location = 'json')
		self.reqparser.add_argument('weekend_only',type = unicode,location = 'json')
		self.reqparser.add_argument('starting_price',type = int,location = 'json')
		self.reqparser.add_argument('ending_price',type = int,location = 'json')
		super(task,self).__init__()
           
	def get(self,task_id = None):
		if task_id == None:
			if g.user.type == 'C':
				customer = CustomerClass.query.filter_by(user_id=g.user.user_id).first()
				tasks = TaskClass.query.filter_by(customer_id=customer.customer_id).all()			
				ServiceProvidersForTasks = {}
				for task in tasks:
					ServiceProvidersForTasks[task.task_id] = self.marshal_items(ServiceProviderInTaskClass.query.filter_by(task_id=task.task_id).all(),serviceProviderInTask_fields)
					for sprov in ServiceProvidersForTasks[task.task_id]:
						new_fields = marshal(ServiceProviderClass.query.filter_by(serviceProvider_id = sprov['serviceProvider_id']).first(),serviceProvider_fields)
						ServiceProvidersForTasks[task.task_id].remove(sprov)
						sprov = dict(sprov.items() + new_fields.items())
						ServiceProvidersForTasks[task.task_id].append(sprov)
				
				ServiceProvidersIDList = set([item['serviceProvider_id'] for key, value in ServiceProvidersForTasks.iteritems() for item in value])
			
				ReviewsForServiceProviders = {}
				for sprov_id in ServiceProvidersIDList:
					ReviewsForServiceProviders[sprov_id] = self.marshal_items(ReviewsClass.query.filter_by(serviceProvider_id = sprov_id).all(), review_fields)
					
				return_obj = {'Tasks': map(lambda x: marshal(x,task_fields),tasks),
							  'ServiceProviders': ServiceProvidersForTasks,
							  'Reviews': ReviewsForServiceProviders}
				return return_obj
			else:
				serviceProvider = ServiceProviderClass.query.filter_by(user_id=g.user.user_id).first()
				TasksForServiceProvider = ServiceProviderInTaskClass.query.filter_by(serviceProvider_id=serviceProvider.serviceProvider_id).all()
				tasks = [TaskClass.query.filter_by(task_id=TaskForServiceProvider.task_id).first() for TaskForServiceProvider in TasksForServiceProvider]
				ServiceProvidersForTasks = {}
				for task in tasks:
					ServiceProvidersForTasks[task.task_id] = []
				if tasks:
					return {'Tasks': map(lambda x: marshal(x,task_fields),tasks), 'ServiceProviders':ServiceProvidersForTasks, 'Reviews': {}}
				else:
					return {'Tasks': [], 'ServiceProviders':ServiceProvidersForTasks, 'Reviews':{}}
			
	def put(self,task_id):  
		try:
			Updated_Task = TaskClass.query.get(int(task_id))
		except NoResultFound:
			abort(404)
					   
		args = self.reqparser.parse_args()  
		received_data = dict([(key,value) for key,value in args.iteritems() if value!= None])
             
		Updated_Task.area = received_data['area']
		Updated_Task.type = received_data['type']
		Updated_Task.service = received_data['service']
		Updated_Task.evening_only = received_data['evening_only']
		Updated_Task.weekend_only = received_data['weekend_only']
		Updated_Task.starting_price = received_data['starting_price']
		Updated_Task.ending_price = received_data['ending_price']
		
		db.session.query(ServiceProviderInTaskClass).filter(ServiceProviderInTaskClass.task_id==task_id).delete()
		db.session.commit()
		serviceProvidersObjList = []
		ReviewsForServiceProviders = {}
		ServiceProviders = ServiceProviderClass.query.filter(ServiceProviderClass.rate_per_hr >= received_data['starting_price'],ServiceProviderClass.rate_per_hr <= received_data['ending_price']).all()
		for serviceProvider in ServiceProviders:   
			areaIdObjs = AreasForUserClass.query.filter_by(user_id = serviceProvider.user_id)
			for areaIdObj in areaIdObjs:
				if AreasClass.query.filter_by(area_id = areaIdObj.area_id).first().area == received_data['area']:
					serviceIdObjs = ServicesByServiceProviderClass.query.filter_by(user_id = serviceProvider.user_id)
					for serviceIdObj in serviceIdObjs:
						if ServicesClass.query.filter_by(service_id = serviceIdObj.service_id).first().service == received_data['service']:
							ServiceProvidersInTaskObj = ServiceProviderInTaskClass(serviceProvider_id = int(serviceProvider.serviceProvider_id), task_id = int(task_id))
							db.session.add(ServiceProvidersInTaskObj)
							ServiceProvidersInTaskObj = marshal(ServiceProvidersInTaskObj,serviceProviderInTask_fields)
							serviceprovObj = marshal(ServiceProviderClass.query.filter_by(serviceProvider_id = serviceProvider.serviceProvider_id).first(),serviceProvider_fields)
							serviceprovObj = dict(ServiceProvidersInTaskObj.items() + serviceprovObj.items())
							serviceProvidersObjList.append(serviceprovObj)
							ReviewsForServiceProviders[serviceProvider.serviceProvider_id] = self.marshal_items(ReviewsClass.query.filter_by(serviceProvider_id = serviceProvider.serviceProvider_id).all(), review_fields)
							db.session.commit()
							break
					break
		return {"Task":marshal(Updated_Task,task_fields),'ServiceProviders':serviceProvidersObjList,'Reviews':ReviewsForServiceProviders}
		                       
	def post(self):  
		customer = CustomerClass.query.filter_by(user_id=g.user.user_id).first()
		args = self.reqparser.parse_args() 
		taskObj = TaskClass(customer_id = customer.customer_id, area = args['area'], type = args['type'], service = args['service'], evening_only = args['evening_only'],weekend_only = args['weekend_only'], starting_price = args['starting_price'], ending_price = args['ending_price'])
		db.session.add(taskObj)
		db.session.flush()
		new_task_id = taskObj.task_id
		db.session.commit()
               
		serviceProvidersObjList = []
		return_obj = {}
		ReviewsForServiceProviders ={}
		ServiceProviders = ServiceProviderClass.query.filter(ServiceProviderClass.rate_per_hr >= args['starting_price'],ServiceProviderClass.rate_per_hr <= args['ending_price']).all()
		for serviceProvider in ServiceProviders:
			areaIdObjs = AreasForUserClass.query.filter_by(user_id = serviceProvider.user_id)
			for areaIdObj in areaIdObjs:
				if AreasClass.query.filter_by(area_id = areaIdObj.area_id).first().area == args['area']:
					serviceIdObjs = ServicesByServiceProviderClass.query.filter_by(user_id = serviceProvider.user_id)
					for serviceIdObj in serviceIdObjs:
						if ServicesClass.query.filter_by(service_id = serviceIdObj.service_id).first().service == args['service']:
							serviceProvidersintaskObj = ServiceProviderInTaskClass(serviceProvider_id = serviceProvider.serviceProvider_id, task_id = new_task_id)
							serviceProvidersObjList.append(serviceProvider)
							ReviewsForServiceProviders[serviceProvider.serviceProvider_id] = self.marshal_items(ReviewsClass.query.filter_by(serviceProvider_id = serviceProvider.serviceProvider_id).all(), review_fields)
							db.session.add(serviceProvidersintaskObj)
							db.session.commit()
							break
					break
		return_obj = {'Task' : marshal(taskObj, task_fields)}
		if serviceProvidersObjList:
			return_obj['ServiceProviders'] = map(lambda x: marshal(x,serviceProvider_fields),serviceProvidersObjList)
			return_obj['Reviews'] = ReviewsForServiceProviders
		else:
			return_obj['ServiceProviders'] = []
			return_obj['Reviews'] = {}
		
		return return_obj
          
def setup_routes():		  
	api.add_resource(task,'/SmartServicesApp/api/v1.0/tasks',endpoint='tasks')        
	api.add_resource(task,'/SmartServicesApp/api/v1.0/tasks/<int:task_id>',endpoint='task')