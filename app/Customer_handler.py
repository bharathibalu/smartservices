from flask.ext.restful import Resource, marshal, fields, reqparse
from app import auth
from app import api
from app.database import CustomerClass
from app.database import AreasClass
from app.database import AreasForUserClass
from flask import g
from app import db
from misc import area_fields

#Fields for sending data to the front-end
customer_fields = {
	'user_id': fields.Integer,
	'customer_id' : fields.Integer,
	'name' : fields.String,
	'gender' : fields.String,
	'phone' : fields.Integer, #will 10 digits fit in integer?
	'alternative_phone' : fields.Integer, #will 10 digits fit in integer?
	'email' : fields.String #will all symbols fit in string? Shouldnt we check for email?
}

class customer(Resource):
	decorators = [auth.login_required]
	def __init__(self):
		#Parse input data from front-end
		self.reqparser = reqparse.RequestParser()
		self.reqparser.add_argument('name',type = unicode,location = 'json')
		self.reqparser.add_argument('gender',type = unicode,location = 'json')
		self.reqparser.add_argument('area',type = list,location = 'json')
		self.reqparser.add_argument('phone',type = int,location = 'json')
		self.reqparser.add_argument('alternative_phone',type = int,location = 'json')
		self.reqparser.add_argument('email',type = unicode,location = 'json')
											
		super(customer,self).__init__()
           
	def get(self):
		try:
			customer = CustomerClass.query.filter_by(user_id=g.user.user_id).first()
			user_areas = AreasForUserClass.query.filter_by(user_id=g.user.user_id).all()
			customer_areas = [AreasClass.query.filter_by(area_id=user_area.area_id).first() for user_area in user_areas]
			customerAreaList = []
			for customer_area in customer_areas:
				customerAreaList.append(customer_area.area)
		except NoResultFound:
			abort(404)
		return {'Customer': marshal(customer,customer_fields),'Customer_Areas':customerAreaList}
                         
	def put(self):  
		try:
			Updated_Customer = CustomerClass.query.filter_by(user_id=g.user.user_id).first()
		except NoResultFound:
			abort(404)
             
		args = self.reqparser.parse_args()  
		received_data = dict([(key,value) for key,value in args.iteritems() if value!= None])
                         
		Updated_Customer.user_id = g.user.user_id
		Updated_Customer.name = received_data['name']
		Updated_Customer.gender = received_data['gender']
		Updated_Customer.phone = received_data['phone']
		Updated_Customer.alternative_phone = received_data['alternative_phone']
		Updated_Customer.email = received_data['email']
               	
		db.session.query(AreasForUserClass).filter(AreasForUserClass.user_id==g.user.user_id).delete()
		db.session.commit()
				
		areasObjList = []
		for area in received_data['area']:
			areasObj = AreasClass.query.filter_by(area=area).first()
			if areasObj == None:
				areasObj = AreasClass(area = area)
				db.session.add(areasObj)
				db.session.flush()
                   
			areasObjList.append(areasObj)
			AreasForUserObj = AreasForUserClass(area_id = areasObj.area_id, user_id = g.user.user_id)
			db.session.add(AreasForUserObj)
		db.session.commit()
		
		New_Customer = CustomerClass.query.all()
		return {"Updated_Customer":marshal(Updated_Customer,customer_fields),'Updated_Areas':map(lambda x: marshal(x,area_fields),areasObjList)}
                       
	def post(self):  
		args = self.reqparser.parse_args()                
		customerObj = CustomerClass(user_id = g.user.user_id, name = args['name'], gender = args['gender'], phone = args['phone'], alternative_phone = args['alternative_phone'], email = args['email'])
		db.session.add(customerObj)
            
		areasObjList = []
		for area in args['area']:
			areasObj = AreasClass.query.filter_by(area=area).first()
			if areasObj == None:
				areasObj = AreasClass(area = area)
				db.session.add(areasObj)
				db.session.flush()
            
			areasObjList.append(areasObj)
			AreasForUserObj = AreasForUserClass(area_id = areasObj.area_id, user_id = g.user.user_id)
			db.session.add(AreasForUserObj)
			
		db.session.commit()         
		return {'Newly_added_Customer' : marshal(customerObj, customer_fields),'Newly_Added_Areas':map(lambda x: marshal(x,area_fields),areasObjList)}
			   
def setup_routes():	
	api.add_resource(customer,'/SmartServicesApp/api/v1.0/customer',endpoint='customers')  		  