app.controller('userController',['$http','$scope','$rootScope',function($http,$scope,$rootScope){
	var self = this;
	$rootScope.userTemplate = { name: 'userView.html', url: 'userView.html'};

	self.user ={};
	self.user.type = 'C';
	
	self.token = {};
	self.user.service = [];
	self.user.area = [];
	$rootScope.authReq = true;
	
	$scope.$on('getDetailsEvent', function(event) {
		$rootScope.displayDetailsReq = true;
		if($rootScope.type == 'C'){
			var url = "http://127.0.0.1:5000/SmartServicesApp/api/v1.0/customer";
		}else{
			var url = "http://127.0.0.1:5000/SmartServicesApp/api/v1.0/serviceProvider";
		}
		$http.defaults.headers.common.Authorization = 'Basic ' + btoa($rootScope.token + ':' + "");
		$http.get(url).success(function(data){
			if($rootScope.type == 'C'){
				self.user = data.Customer;
				self.user.area = data.Customer_Areas;
			}else{
				self.user = data.ServiceProvider;
				self.user.area = data.ServiceProvider_areas;
				self.user.service = data.ServiceProvider_services;
			}
			}).error(function(data){
			$rootScope.$broadcast('authReqEvent', true);
		});
	});
	
	$scope.$on('authReqEvent', function(event, data) {
		$rootScope.authReq = data;
	});
	
	self.viewPersonalDetails = function(){
	}
	
	self.firstTimeUser=function(){  
		$scope.newUserReq=true;
	};
	
	self.PostUser = function(){
		if(self.user.type == 'C'){
			var url = "http://127.0.0.1:5000/SmartServicesApp/api/v1.0/customer";
		}else{
			var url = "http://127.0.0.1:5000/SmartServicesApp/api/v1.0/serviceProvider";
			self.addItems(self.user.service,self.user.new_service);
		}
		self.addItems(self.user.area,self.user.new_area);
		$http.post(url,self.user).success(function(data){
		}).error(function(data){
			$scope.detailsIncorrect = true;
			self.user ={};
			self.user.type = 'C';
		});
	};
	
	self.signUpUser = function(){
		$rootScope.type = self.user.type;
		$http.post("http://127.0.0.1:5000/SmartServicesApp/api/v1.0/users",self.user).success(function(data){
			self.authenticate();
			}).error(function(data){
			$scope.signUpIncorrect = true;
			self.user ={};
			self.user.type = 'C';
		});
	};
	
	self.addItems = function(area,new_area) {
		area.push(new_area);
	};
	self.beginEditDetails= function(){
		$rootScope.displayDetailsReq = false;
		$rootScope.editDetails = true;
	}
	self.Cancel = function(){
		$rootScope.authReq = false;
		$rootScope.editDetails = false;
	}
	self.displayCancel = function(){
		$rootScope.displayDetailsReq = false;
	}
	
	self.editUserDetails = function(){
		if($rootScope.type == 'C'){
			var url = "http://127.0.0.1:5000/SmartServicesApp/api/v1.0/customer";
		}else{
			var url = "http://127.0.0.1:5000/SmartServicesApp/api/v1.0/serviceProvider";
		}
		$http.defaults.headers.common.Authorization = 'Basic ' + btoa($rootScope.token + ':' + "");
		$http.put(url,self.user).success(function(data){
			$rootScope.editDetails = false;
		}).error(function(data){
			$rootScope.editDetails = true;
		});
	}
	
	self.authenticate = function(){
		$rootScope.type = self.user.type
		$http.defaults.headers.common.Authorization = 'Basic ' + btoa(self.user.username + ':' + self.user.password);
		$http.get("http://127.0.0.1:5000/SmartServicesApp/api/v1.0/token").success(function(data){
			
			$rootScope.token = data.token;
			$rootScope.authReq = false;
			if ($scope.newUserReq==true){
				$scope.newUserReq=false;
				self.PostUser();
			}
			self.user ={};
			$rootScope.$broadcast('getTaskEvent');
			}).error(function(data){
			$scope.LoginIncorrect = true;
			self.user ={};
			self.user.type = 'C';
		});
	};        
}]);        