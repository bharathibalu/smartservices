app.controller('taskController',['$http','$scope','$rootScope',function($http,$scope,$rootScope){
	var self = this;
	$rootScope.taskTemplate = { name: 'taskView.html', url: 'taskView.html'};
	
	self.tasks = [];
	self.current_task={};
	self.editedTask={};
	self.tasks.serviceProviders = [];
	self.tasks.serviceProviders.reviews = [];
	
	$scope.$on('getTaskEvent', function(event) {
		$http.defaults.headers.common.Authorization = 'Basic ' + btoa($rootScope.token + ':' + "");
		$http.get('/SmartServicesApp/api/v1.0/tasks').success(function(data){
			self.tasks = data.Tasks
			for (i = 0; i < self.tasks.length; i++){
				self.tasks[i].serviceProviders = data.ServiceProviders[self.tasks[i].task_id];
				for (j = 0; j < self.tasks[i].serviceProviders.length; j++){
					self.tasks[i].serviceProviders[j].reviews = data.Reviews[self.tasks[i].serviceProviders[j].serviceProvider_id];
				}
			}
		}).error(function(data){
			$rootScope.$broadcast('authReqEvent', true);
		});
	});
	
	self.viewPersonalDetails = function(){
		$rootScope.$broadcast('getDetailsEvent');
	}
	self.beginAdd=function(){
		$scope.addTaskReq=true;
	};
	
	self.addTask=function(){
		$scope.addTaskReq=false;
		$http.defaults.headers.common.Authorization = 'Basic ' + btoa($rootScope.token + ':' + "");
		$http.post('/SmartServicesApp/api/v1.0/tasks',self.current_task).success(function(data){
			self.tasks.push(data.Task);
			self.tasks[self.tasks.length-1].serviceProviders = data.ServiceProviders;
			for (j = 0; j < self.tasks[self.tasks.length-1].serviceProviders.length; j++){
				self.tasks[self.tasks.length-1].serviceProviders[j].reviews = data.Reviews[self.tasks[self.tasks.length-1].serviceProviders[j].serviceProvider_id];
			}
			self.current_task={};
			}).error(function(data){
			$rootScope.$broadcast('authReqEvent', true);
		});
	};
	
	self.beginEdit=function(editedTask){
		//To populate the edit pop-up with data
		self.current_task = editedTask;        
		$scope.editTaskReq=true;
		self.editedTask=editedTask;
	};
	
	self.editTask=function(){
		$scope.editTaskReq=false;
		$http.defaults.headers.common.Authorization = 'Basic ' + btoa($rootScope.token + ':' + "");
		$http.put('/SmartServicesApp/api/v1.0/tasks/'+ self.editedTask.task_id,self.current_task).success(function(data){
			self.updateTask(self.editedTask,data.Task,data.ServiceProviders, data.Reviews);
			self.editedTask ={};
			//To clear out the Edit pop-up data
			self.current_task = {};
			}).error(function(data){
			$rootScope.$broadcast('authReqEvent', true);
		});
	};
	
	self.updateTask= function(task, Updated_Task, ServiceProviders, Reviews){
		var i= self.tasks.indexOf(task);
		self.tasks[i].service =Updated_Task.service;
		self.tasks[i].area= Updated_Task.area;
		self.tasks[i].type = Updated_Task.type;
		self.tasks[i].evening_only = Updated_Task.evening_only;
		self.tasks[i].weekend_only = Updated_Task.weekend_only;
		self.tasks[i].starting_price = Updated_Task.starting_price;
		self.tasks[i].ending_price = Updated_Task.ending_price;
		self.tasks[i].serviceProviders = ServiceProviders;
		for (j = 0; j < self.tasks[i].serviceProviders.length; j++){
			self.tasks[i].serviceProviders[j].reviews = Reviews[self.tasks[i].serviceProviders[j].serviceProvider_id];
		}
	};
	self.markInProgress = function(editedTask){
		//To pass the entire tasks to the server for PUT request
		self.current_task = editedTask;
		self.current_task['Done']=false;
		
		self.editedTask = editedTask;
		$http.defaults.headers.common.Authorization = 'Basic ' + btoa($rootScope.token + ':' + "");
		$http.put(self.editedTask.uri,self.current_task).success(function(data){
			self.updateTask(self.editedTask,data.Updated_Task);
			self.editedTask ={};
			//To clear out the Edit pop-up data
			self.current_task = {};
			}).error(function(data){
			$rootScope.$broadcast('authReqEvent', true);
		});
	};
	
	self.markDone = function(editedTask){
		//To pass the entire task to the server for PUT request
		self.current_task = editedTask;
		self.current_task['Done']=true;
		
		self.editedStudent = editedTask;
		$http.defaults.headers.common.Authorization = 'Basic ' + btoa($rootScope.token + ':' + "");
		$http.put(self.editedTask.uri,self.current_task).success(function(data){
			self.updateTask(self.editedTask,data.Updated_Task);
			self.editedTask ={};
			//To clear out the Edit pop-up data
			self.current_task = {};
			}).error(function(data){
			$rootScope.$broadcast('authReqEvent', true);
		});
	};
	self.deleteTask = function(editedTask){
		$http.defaults.headers.common.Authorization = 'Basic ' + btoa($rootScope.token + ':' + "");
		$http.delete(editedTask.uri).success(function(data){
			var i= self.tasks.indexOf(editedTask);
			self.tasks.splice(i,1);
			}).error(function(data){
			$rootScope.$broadcast('authReqEvent', true);
		});
	};        
}]);